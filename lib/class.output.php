<?php

namespace output;

class output {

    private $output = null; /// xml | print;
    private $data = [];
    private $nodeName = 'data';

    private $allowedType = ['print','xml','json'];

    // prepare the output data
    //// $strName string | nodename
    //// $arrData array # [tag=>value]
    //// return void
    public function setData($strName,$arrData)
    {
        if(!isset($this->data[$strName])){
            $this->data[$strName] = [];
        }
        $this->data[$strName][] = $arrData;
    }


    // prepare the output type
    // $output string # print | json | xml
    // return void
    public function setOutputType($output)
    {
        // never overwrite the expeced output!
        if($this->output && $this->output !== $output ){
            trigger_error("info#output is alraedy defend. Expected output is '".$this->output ."', you can't change it to '".$output."'",E_USER_ERROR); // db global info massage;
        }
        // only  print | json | xml is allowed
        if(!in_array($output, $this->allowedType)){
            trigger_error("info#output '".$output."' is not allowed. use one of '". implode(',',$this->allowedType)."'",E_USER_ERROR); // db global info massage;
        }
        $this->output = $output;
    }

    // get the output type
    // $output string # print | json | xml
    // return output type
    public function getOutputType(){
        // if output type is not set use json as default
        if(!$this->output){
            $this->output = 'json';
        }
        return  $this->output;
    }


    // print the result  to the screen
    public function display(){
        // data as to be fulled
        if(!$this->data){
            return false;
        }
        // get output type
        $output = $this->getOutputType();
        switch ($output){
            case 'xml':
                return $this->printXML();
                break;
            case 'json':
                return $this->printJSON();
                break;
            default:
                // if not defend use this as debug info
                $this->printData();
                break;
        }
    }

    //this is used for debuging
    private function printData()
    {
        print_r($this->data);
    }

    // return the data as JSON data
    private function printJSON()
    {
        return json_encode($this->data);
    }

    // return the data as XML data
    private function printXML()
    {
        // if there is one node make this as root node, otherwise use the default nodename (data)
        $firstNode = (count($this->data) > 1 ? $this->nodeName : array_key_first($this->data));
        $xml = new \SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><".$firstNode." />");
        $xmlNode = $xml;

        foreach ($this->data as $nodeName => $arrData) {
            if(count($this->data) > 1 ) {
                // overwite the xmlNode if there are more then one output data
                $xmlNode = $xml->addChild($nodeName);
            }
            // set the amount on the last creaded node
            $xmlNode->addAttribute('amount', count($arrData));
            // create a result node
            $xmlResult = $xmlNode->addChild('result');
            // data loop untill the value is not a array
            $xmlLoop = $this->loopXMLNodes($xmlResult,$arrData);
        }
        return $xml->asXML();
    }

    // data loop untill the value is not a array
    private function loopXMLNodes($nodeXml,$arrNodeData){
        foreach ($arrNodeData as $keyNode => $arrNodeValue) {
            //$xmlLoop = $nodeXml->addChild($keyNode,$value);
            if(is_array($arrNodeValue)) {
                $xmlLoop = $this->loopXMLNodes($nodeXml, $arrNodeValue);
                continue;
            }
            $xmlLoop = $nodeXml->addChild($keyNode,$arrNodeValue);
        }
        return $xmlLoop;
    }


};
